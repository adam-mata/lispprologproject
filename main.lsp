(defparameter db nil)

(defun create-person(id mother-id father-id year-of-birth year-of-death job savings)
	(push (list :person-id id :mother-id mother-id :father-id father-id :year-of-birth year-of-birth
		:year-of-death year-of-death :job job :savings savings) db)
)


(defun add-to-db(person)
	(push person db)
)

(defun init-db()
	(create-person 0 -1 -1 1930 2000 "doctor" 2000)
	(create-person 1 -1 -1 1935 2000 "cook" 3000)
	(create-person 2 0 1 1960 2020 "policeman" 1000)
	(create-person 3 0 1 1965 -1 "doctor" 2500)
	(create-person 4 -1 -1 1955 2015 "astronaut" 6000)
	(create-person 5 2 4 1985 -1 "programmer" 4000)
	(create-person 6 -1 -1 1940 2010 "writer" 1000)
	(create-person 7 -1 -1 1940 -1 "engineer" 3000)
	(create-person 8 6 7 1965 -1 "cook" 2000)
	(create-person 9 8 -1 1990 -1 nil 0)
	(create-person 10 2 -1 1985 -1 "teacher" 2000)
)

(defun print-db ()
  (dolist (p db)
    (format t "~{~a:~a~%~}~%" p))
)

(defun get-all-descendants(id)
	(defparameter temp nil)
	(dolist (p db temp)
		(let ((mother-id (getf p :mother-id))
		  (father-id (getf p :father-id)))
		    ;get descendants if this is father
			(if (= id father-id)
				(let ((person-id (getf p :person-id)))
					(push person-id temp)
					;(format t "~%~A" person-id)
					(setf temp (append temp (get-all-descendants person-id)))
				)
			)
			;get descendants if this is mother
			(if (= id mother-id)
				(let ((person-id (getf p :person-id)))
					(push person-id temp)
					;(format t "~%~A" person-id)
					(setf temp (append temp (get-all-descendants person-id)))
				)
			)
		)
	)
)

(defun get-by-id(id)
    (dolist (p db)
        (let 
            (
                (person-id (getf p :person-id))
            )
            (if (eq person-id id)
                (return-from get-by-id p)
            )
        )
    )
)

(defun get-all-ancestors(id)
    (defparameter temp nil)
    (let
        (
            (father-id (getf (get-by-id id) :father-id))
            (mother-id (getf (get-by-id id) :mother-id))
        )
        (if (not (eq father-id -1))
            (progn
                ;(format t "~%~A" father-id)
                (push father-id temp)
                (setf temp (append temp (get-all-ancestors father-id)))
            )
        )
        (if (not (eq mother-id -1))
            (progn
                ;(format t "~%~A" mother-id)
                (push mother-id temp)
                (setf temp (append temp (get-all-ancestors mother-id)))
            )
        )
    )
)

(defun is-ancestor-of(anc-id dsc-id)
    (if (eq t (numberp (position anc-id (get-all-ancestors dsc-id))))
        (return-from is-ancestor-of t)
    )
    (not t)
)

(defun get-year()
	(multiple-value-bind (second minute hour date month year day-of-week dst-p tz) (get-decoded-time)
		(return-from get-year year)
	)
)

(defun print-ancestors-life-lengths(id)
	(defparameter person (get-by-id id))
	(if (not (eq person nil))
		(let ((current-year (get-year))
			(year-of-death (getf person :year-of-death))
			(year-of-birth (getf person :year-of-birth)))
			(if (eq year-of-death -1)
				(defparameter age (- current-year year-of-birth))
				(defparameter age (- year-of-death year-of-birth))
			)
			(format t "~A:~A~%" id age)
			(let ((mother-id (getf person :mother-id))
			  (father-id (getf person :father-id)))
				(print-ancestors-life-lengths mother-id)
				(print-ancestors-life-lengths father-id)
			)
		)
	)
)

(defun have-common-ancestor(id1 id2)
	(defparameter id1-ancestors (get-all-ancestors id1))
	(defparameter id2-ancestors (get-all-ancestors id2))
	(push id1 id1-ancestors)
	(push id2 id2-ancestors)
	(format t "Have common ancestor: ~A~%" 
		(not (eq (intersection id1-ancestors id2-ancestors) nil)))
)

(defun print-savings-of-the-blood-line-interior(person_idx_list)
    (dolist (id person_idx_list)
        (let 
            (
                (person-id (getf (get-by-id id) :person-id))
                (savings (getf (get-by-id id) :savings))
            )
            (print (concatenate 'string "Person with ID " (write-to-string person-id) " has savings of amount $" (write-to-string savings) "."))
        )
    )
)

(defun get-oldest-ancestor(id)
    (defparameter ancestors (get-all-ancestors id))
    (defparameter oldest nil)
    (defparameter oldest-year-of-birth -1)
    (dolist (p ancestors oldest)
        (let ((current (get-by-id p)))
			(if (eq oldest nil)			
				(setf oldest p
						oldest-year-of-birth (getf current :year-of-birth)
				)			
				(if (< (getf current :year-of-birth) oldest-year-of-birth)
					(setf oldest p
				   oldest-year-of-birth (getf current :year-of-birth)
					)
				)
			)
        )
    )
)



(defun get-youngest-descendant(id)
    (defparameter descendants (get-all-descendants id))
    (defparameter youngest nil)
    (defparameter youngest-year-of-birth -1)
    (dolist (p descendants youngest)
        (let ((current (get-by-id p)))
			(if (eq youngest nil)			
				(setf youngest p
					  youngest-year-of-birth (getf current :year-of-birth)
				)			
				(if (> (getf current :year-of-birth) youngest-year-of-birth)
					(setf youngest p
				          youngest-year-of-birth (getf current :year-of-birth)
					)
				)
			)
        )
    )
)


(defun descendants-who-works-as(id job-name)
	(defparameter temp nil)
	(dolist (p db temp)
		(let ((mother-id (getf p :mother-id))
		  (father-id (getf p :father-id))
              (job (getf p :job)))
		    ;get descendants if this is father && it is his job
			(if (= id father-id)
				(let ((person-id (getf p :person-id)))
					(if (string= job-name job)
                     (push person-id temp)
					;(format t "~%~A" person-id)
					(setf temp (append temp (get-all-descendants person-id)))
				))
			)
			;get descendants if this is mother and this is his job
			(if (= id mother-id)
				(let ((person-id (getf p :person-id)))
                     (if (string= job-name job)
					(push person-id temp)
					;(format t "~%~A" person-id)
					(setf temp (append temp (get-all-descendants person-id)))
				)
			))
		)
	)
)


(defun print-savings-of-the-blood-line(id)
    (print "Savings of the ancestors:")
    (print-savings-of-the-blood-line-interior (get-all-ancestors id))
    (print "Savings of the descendants:")
    (print-savings-of-the-blood-line-interior (get-all-descendants id))
)
;; ----------------------------------------- ;;
;; ----------------MAKRA-------------------- ;;
;; ----------------------------------------- ;;

(defmacro filter-ancestors-by-job(id job condition)
   `(let
        (
            (father-id (getf (get-by-id ,id) :father-id))
            (mother-id (getf (get-by-id ,id) :mother-id))
        )
        (if (not (eq father-id -1))
            (let
                (
                    (father-job (getf (get-by-id father-id) :job))
                )
                (if (,condition ,job father-job)
                    (print (concatenate 'string "Person with ID " (write-to-string father-id) " works as a " father-job))
                )
                ;tutaj odpalić makro
                (filter-ancestors-by-job father-id ,job ,condition)
            )
        )
        (if (not (eq mother-id -1))
            (let
                (
                    (mother-job (getf (get-by-id mother-id) :job))
                )
                (if (,condition ,job mother-job)
                    (print (concatenate 'string "Person with ID " (write-to-string mother-id) " works as a " mother-job))
                )
                ;tutaj odpalić makro
                (filter-ancestors-by-job mother-id ,job ,condition)
            )
        )
    )
)


(defmacro filter-by-descendant-savings (id savings condition)
	(defparameter temp nil)
	`(dolist (p db temp)
		(let ((mother-id (getf p :mother-id))
		  (father-id (getf p :father-id)))
		    ;get descendants if this is father
			(if (= ,id father-id)
				(let ((person-id (getf p :person-id)))
					(if (,condition (getf p :savings) ,savings)
						(push person-id temp)
					)
					(setf temp (append temp (filter-by-descendant-savings person-id ,savings, condition)))
				)
			)
			;get descendants if this is mother
			(if (= ,id mother-id)
				(let ((person-id (getf p :person-id)))
					(if (,condition (getf p :savings) ,savings)
						(push person-id temp)
					)
					;(format t "~%~A" person-id)
					(setf temp (append temp (filter-by-descendant-savings person-id ,savings ,condition)))
				)
			)
		)
	)
)

(defmacro modify-ancestors-savings(id mod-value operation)
    `(let ((father-id (getf (get-by-id ,id) :father-id))
		(mother-id (getf (get-by-id ,id) :mother-id)))
        (if (not (eq father-id -1))
            (let((father (get-by-id father-id)))
				(setf (getf father :savings) (,operation (getf father :savings) ,mod-value))
				(modify-ancestors-savings father-id ,mod-value ,operation)
			)
        )
        (if (not (eq mother-id -1))
			(let((mother (get-by-id mother-id)))
				(setf (getf mother :savings) (,operation (getf mother :savings) ,mod-value))
				(modify-ancestors-savings mother-id ,mod-value ,operation)	
			)
		)
    )
)

(defmacro kill-all-descendants(id year)
	`(dolist (p db)
		(let ((mother-id (getf p :mother-id))
		  (father-id (getf p :father-id)))
			(if (= ,id father-id)
				(let ((person-id (getf p :person-id)))
					(setf (getf p :year-of-death) ,year)
					(kill-all-descendants person-id ,year)
				)
			)
			(if (= ,id mother-id)
				(let ((person-id (getf p :person-id)))
					(setf (getf p :year-of-death) ,year)
					(kill-all-descendants person-id ,year)
				)
			)
		)
	)
)

(defmacro kill-all-ancestors(id year-of-death)
   `(let
        (
            (father-id (getf (get-by-id ,id) :father-id))
            (mother-id (getf (get-by-id ,id) :mother-id))
        )
        
        (if (not (eq father-id -1))
            (progn
                ;wydrukować info o zmianie
                (print (concatenate 'string "Killing person with ID " (write-to-string father-id) ". Setting year of death to " (write-to-string ,year-of-death) "." ))
                ;dokonac zmiany
                (setf (getf (get-by-id father-id) :year-of-death) ,year-of-death)
                ;tutaj odpalić makro rek
                (kill-all-ancestors father-id ,year-of-death)
            )
        )
        
        (if (not (eq mother-id -1))
            (progn
                ;wydrukować info o zmianie
                (print (concatenate 'string "Killing person with ID " (write-to-string mother-id) ". Setting year of death to " (write-to-string ,year-of-death) "." ))
                ;dokonac zmiany
                (setf (getf (get-by-id mother-id) :year-of-death) ,year-of-death)
                ;tutaj odpalić makro rek
                (kill-all-ancestors mother-id ,year-of-death)
            )
        )
    )
)


(defmacro modify-descendants-savings(id mod-value operator)
   `(dolist (p db)
        (let (
                (father-id (getf p :father-id))
                (mother-id (getf p :mother-id))
                (person-id (getf p :person-id))
             )
             (if (or (eq father-id ,id) (eq mother-id ,id))
                 (let (
                        (person-savings (getf (get-by-id person-id) :savings))
                      )
                      (setf (getf (get-by-id person-id) :savings) (,operator person-savings ,mod-value))
                      (modify-descendants-savings person-id ,mod-value ,operator)
                 )
             )
        )
    )
)

(defmacro get-older-ancestor(person-1-id person-2-id)
    `(let ((ancestor1 (get-oldest-ancestor ,person-1-id))
		   (ancestor2 (get-oldest-ancestor ,person-2-id))
           (oldest nil)) 
		(if (not (eq ancestor1 nil))
			(progn (setf oldest ancestor1)
				(if (and (not (eq ancestor2 nil))
					(> (getf (get-by-id ancestor1) :year-of-birth)
					   (getf (get-by-id ancestor2) :year-of-birth)))
					(setf oldest ancestor2)
					(setf oldest ancestor1)
				)
			)
			(if (not (eq ancestor2 nil))
				(setf oldest ancestor2)
			)
		)
	)
)


(defmacro filter-by-ancestor-savings(id savings condition)
   `(let
        (
            (father-id (getf (get-by-id ,id) :father-id))
            (mother-id (getf (get-by-id ,id) :mother-id))
        )
        (if (not (eq father-id -1))
            (let
                (
                    (father-savings (getf (get-by-id father-id) :savings))
                )
                (if (,condition ,savings father-savings)
                    (print (concatenate 'string "Person with ID " (write-to-string father-id) " has savings " (write-to-string father-savings)))
                    ;(print " has savings " father-savings)
                )
                ;tutaj odpalić makro
                (filter-by-ancestor-savings father-id ,savings ,condition)
            )
        )
        (if (not (eq mother-id -1))
            (let
                (
                    (mother-savings (getf (get-by-id mother-id) :savings))
                )
                (if (,condition ,savings mother-savings)
                    (print (concatenate 'string "Person with ID " (write-to-string mother-id) " has savings " (write-to-string mother-savings)))
                )
                ;tutaj odpalić makro
                (filter-by-ancestor-savings mother-id ,savings ,condition)
            )
        )
    )
)


(defmacro filter-ancestors-by-year-of-death(id year-of-death condition)
   `(let
        (
            (father-id (getf (get-by-id ,id) :father-id))
            (mother-id (getf (get-by-id ,id) :mother-id))
        )
        (if (not (eq father-id -1))
            (let
                (
                    (father-year-of-death (getf (get-by-id father-id) :year-of-death))
                )
                (if (,condition ,year-of-death father-year-of-death)
                    (print (concatenate 'string "Person with ID " (write-to-string father-id) " died in " (write-to-string father-year-of-death)))
                )
                ;tutaj odpalić makro
                (filter-ancestors-by-year-of-death father-id ,year-of-death ,condition)
            )
        )
        (if (not (eq mother-id -1))
            (let
                (
                    (mother-year-of-death (getf (get-by-id mother-id) :year-of-death))
                )
                (if (,condition ,year-of-death mother-year-of-death)
                    (print (concatenate 'string "Person with ID " (write-to-string mother-id) " died in " (write-to-string mother-year-of-death)))
                )
                ;tutaj odpalić makro
                (filter-ancestors-by-year-of-death mother-id ,year-of-death ,condition)
            )
        )
    )
)


;; ----------------------------------------- ;;
;; ----------------------------------------- ;;
;; ----------------------------------------- ;;

(init-db)
(print-db)
(format t "~%get-all-descendants 0")
(format t "~%RESULT ~%~A" (get-all-descendants 0))

(format t "~%get-all-descendants 6")
(format t "~%RESULT ~%~A" (get-all-descendants 6))

(print "get-all-ancestors 9")
(print (get-all-ancestors 9))

(print "is-ancestor-of 10 9")
(print (is-ancestor-of 10 9))

(print "is-ancestor-of 8 9")
(print (is-ancestor-of 8 9))

(print "print-ancestors-life-lengths 3")
(print-ancestors-life-lengths 3)

(print "have-common-ancestor 10 3")
(have-common-ancestor 10 3)

(print "have-common-ancestor 6 2")
(have-common-ancestor 6 2)

(print "print-savings-of-the-blood-line 7")
(print-savings-of-the-blood-line 7)

(print "filter-ancestors-by-job 5 z string-greaterp")
(filter-ancestors-by-job 5 "z" string-greaterp)

(print "filter-by-descendant-savings 0 2000 >=")
(print (filter-by-descendant-savings 0 2000 >=))

(print "modify-ancestors-savings 2 10 -")
(modify-ancestors-savings 2 10 -)
(print (getf (nth 0 db) :savings))
(print (getf (nth 1 db) :savings))

(print "kill-all-descendants 0 2020")
(kill-all-descendants 0 2020)
(print (getf (nth 5 db) :year-of-death))
(print-db)

(print "kill-all-ancestors 8 9999")
(kill-all-ancestors 8 9999)
(print-db)

(print "modify-descendants-savings 8 666 +")
(modify-descendants-savings 8 666 +)
(print-db)

(print "get-oldest-ancestor 3")
(print (get-oldest-ancestor 3)) 

(print "get-youngest-descendant 7")
(print (get-youngest-descendant 7))

(print "descendants-who-works-as 7 cook")
(print (descendants-who-works-as 7 "cook")) 

(print "filter-by-ancestor-savings 8 3000 >=") 
(filter-by-ancestor-savings 8 3000 >=)

(print "filter-ancestors-by-year-of-death 5 2000 <=")
(filter-ancestors-by-year-of-death 5 2000 <=)

(print "get-the-oldest-ancestor 5 1") 
(print(get-older-ancestor 5 1))

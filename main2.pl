:-dynamic mother/2, father/2.


%mother(0,2) 0 jest matka 2
?-assert(mother(0,2)).
?-assert(mother(0,3)).
?-assert(father(1,2)).
?-assert(father(1,3)).
?-assert(mother(2,5)).
?-assert(father(4,5)).
?-assert(mother(6,8)).
?-assert(father(7,8)).
?-assert(mother(8,9)).
?-assert(mother(2,10)).

?-assert(person(0,1930,2000,"doctor",2000)).
?-assert(person(1,1935,2000,"cook",3000)).
?-assert(person(2,1960,2020,"policeman",1000)).
?-assert(person(3,1965,-1,"doctor",2500)).
?-assert(person(4,1955,2015,"astronaut",6000)).
?-assert(person(5,1985,-1,"programmer",4000)).
?-assert(person(6,1940,2010,"writer",1000)).
?-assert(person(7,1940,-1,"engineer",3000)).
?-assert(person(8,1965,-1,"cook",2000)).
?-assert(person(9,1990,-1,"",0)).
?-assert(person(10,1985,-1,"teacher",2000)).

descendant(X,Y):-mother(Y,X).
descendant(X,Y):-mother(E,X),descendant(E,Y).
descendant(X,Y):-father(Y,X).
descendant(X,Y):-father(E,X),descendant(E,Y).

ancestor(X, Y):-mother(X, Y).
ancestor(X, Y):-mother(X, E),ancestor(E, Y).
ancestor(X, Y):-father(X, Y).
ancestor(X, Y):-father(X, E),ancestor(E, Y).

getAllDescendants(Id, X):-
	findall(Desc, descendant(Desc, Id), Tmp),
	list_to_set(Tmp,X).
	

getAllAncestors(Id, X):-
	findall(Ascs, ancestor(Ascs, Id), Tmp),
	list_to_set(Tmp,X).

haveCommonAncestor(Id1, Id2):-
	getAllAncestors(Id1, X),
	getAllAncestors(Id2, Y),
	append(X, [Id1], X1),
	append(Y, [Id2], Y1),
	intersection(X1, Y1, CommonAncestors),
	nth0(0, CommonAncestors, _).

getYear(X):-
	get_time(Stamp),
	stamp_date_time(Stamp, DateTime, local),
	date_time_value(year, DateTime, X).

getAge(Birth, Death, Age):-
	Death =:= -1,
	getYear(Now),
	Age is Now - Birth.
getAge(Birth, Death, Age):-
	Age is Death - Birth.

printAncestorsLifeLengths([]).
printAncestorsLifeLengths([H|T]):-
	person(H, Birth, Death, _, _),
	getAge(Birth, Death, Age),
	writef("%w lived %w", [H, Age]),
	nl,
	printAncestorsLifeLengths(T).
printAncestorsLifeLengths(Id):-
	getAllAncestors(Id, Ancestors),
	printAncestorsLifeLengths(Ancestors).

isAncestorOf(Id1, Id2):-
	getAllAncestors(Id2, X),
	intersection(X, [Id1], CommonAncestors),
	nth0(0, CommonAncestors, _).

printSavingsOfList([]).
printSavingsOfList([H|T]):-
	person(H, _, _, _, Money),
	writef("Person %w saved %w dollars.", [H, Money]),
	nl,
	printSavingsOfList(T).
printSavingsOfTheBloodLine(Id):-
	writef("Savings of Ancestors: ", []),
	nl,
	getAllAncestors(Id, Ancestors),
	printSavingsOfList(Ancestors),
	writef("Savings of Descendants: ", []),
	nl,
	getAllDescendants(Id, Descendants),
	printSavingsOfList(Descendants).

getOldestAncestor(X, Xage, MinAge, []).
getOldestAncestor(X, Xage, MinAge, [H|T]):-
	person(H, Birth, Death, _, _),
	getAge(Birth, Death, Age),
	(MinAge < Age ->
		Xage is Age,
		X is H
		;true
	),
	getOldestAncestor(X, Xage, Xage, T).
getOldestAncestor(Id, X, Xage):-
	getAllAncestors(Id, Ancestors),
	MinAge is 0,
	getOldestAncestor(X, Xage, MinAge, Ancestors).


getYoungestDescendant(X, Xage, MaxAge, []).
getYoungestDescendant(X, Xage, MaxAge, [H|T]):-
	person(H, Birth, Death, _, _),
	getAge(Birth, Death, Age),
	(MaxAge > Age ->
		Xage is Age,
		X is H
		;true
	),
	getYoungestDescendant(X, Xage, Xage, T).
getYoungestDescendant(Id, X, Xage):-
	getAllDescendants(Id, Descendants),
	MaxAge is 9999,
	getYoungestDescendant(X, Xage, MaxAge, Descendants).



printDescendantsJob([]).
printDescendantsJob([H|T]):-
	person(H, _, _, Job, _),
	writef("Person %w job %w", [H, Job]),
	nl,
	printDescendantsJob(T).

descendantsWhoWork(Id, job):-
	getAllDescendants(Id, Descendants),
	printDescendantsJob(Descendants).
	


getOldest(X, Job, []).
getOldest(X, Job, [H|T]):-
	person(H, _, _, Work, _),
	(Work == Job ->
		writef("Person %w job %w", [H, Job]),
		X is H
		;true
	),
	getOldest(X, Job, T).
getOldest(Id, X, Job):-
	getAllAncestors(Id, Ancestors),
	getOldest(X, Job, Ancestors).


